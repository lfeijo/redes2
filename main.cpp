//
// Created by Lucas Feijo on 6/5/16.
//

#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <cstring>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <cstdlib>
#include "packets/TCPPacket.h"
#include "packets/IPv6Packet.h"
#include "packets/EthPacket.h"
#include <net/if.h>


#define BUFFSIZE 1050
#define DEFAULT_SOURCE_PORT 1500
#define PACKET_FULL_LEN 74
#define DEFAULT_TIMEOUT 2

char buffer[BUFFSIZE];

// buffer de recv
int socksend;
int sockrecv;
sockaddr_ll *defaultSockAddr;

char mac_dest[6] = "";               //a4:1f:72:f5:90:a8
char ip_dest[INET6_ADDRSTRLEN]  = "";//fe80:0000:0000:0000:a61f:72ff:fef5:90a8
char mac_sour[6] = "";               //00:0a:f7:16:d8:6b
char ip_sour[INET6_ADDRSTRLEN]  = "";//fe80:0000:0000:0000:020a:f7ff:fe16:d86b

char* defaultInterface = "eth1";
struct ifreq ifr;

EthPacket * recebePacote();

void criaSockets(){
    if((socksend = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        perror("Erro na criacao do socket send.\n");
        exit(1);
    }
    if((sockrecv = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        perror("Erro na criacao do socket send.\n");
        exit(1);
    }
}

void enviaPacote(EthPacket *packet){

    char * temp = packet->getPacket();
    if (sendto(socksend, temp, PACKET_FULL_LEN, 0, (struct sockaddr *)(defaultSockAddr), sizeof(struct sockaddr_ll)) < 0){
        perror("erro ao enviar");
    }
}

EthPacket * recebePacote() {
    //char *buff = (char*)malloc(BUFFSIZE);
    int size = recv(socksend, buffer, BUFFSIZE, 0x0);
    if (size<0) {
        perror("erro ao receber");
    }
    EthPacket *pkt = new EthPacket(buffer, size);
    return pkt;
}

void enviaTCPConnect(int start, int end) {

    for(int i = start; i<end; i++) {
        printf("Attack: TCP Connect at %d... ", i);

        TCPPacket *tcpPacket = new TCPPacket(DEFAULT_SOURCE_PORT, i, 0, 0);
        tcpPacket->setSyn(true);
        IPv6Packet *ipv6Packet = new IPv6Packet(ip_dest, ip_sour, tcpPacket);
        EthPacket *ethPacket = new EthPacket(mac_dest, mac_sour, ipv6Packet);
        // SEND SYN
        enviaPacote(ethPacket);

        long timeBefore = time(NULL);
        while(1) {
            // RECV
            EthPacket *receivedPacket = recebePacote();
            // IF SYN/ACK, same ports, same mac
            if (receivedPacket->iPv6Packet != NULL) { // eh ipv6
                if (receivedPacket->iPv6Packet->tcpPacket != NULL) { // eh tcp
                    if (receivedPacket->iPv6Packet->tcpPacket->isSyn() &&
                        receivedPacket->iPv6Packet->tcpPacket->isAck()&&
                        receivedPacket->isSameSource(mac_dest) &&
                        receivedPacket->iPv6Packet->tcpPacket->getSourcePort() == i &&
                        receivedPacket->iPv6Packet->tcpPacket->getDestinationPort() == DEFAULT_SOURCE_PORT) {

                        //SEND ACK
                        tcpPacket = new TCPPacket(DEFAULT_SOURCE_PORT, i,
                                                  receivedPacket->iPv6Packet->tcpPacket->getAckNumber(), 0);
                        tcpPacket->setAck(true);
                        ipv6Packet = new IPv6Packet(ip_dest, ip_sour, tcpPacket);
                        ethPacket = new EthPacket(mac_dest, mac_sour, ipv6Packet);

                        enviaPacote(ethPacket);
                        printf("success.\n");
                        delete receivedPacket;
                        break;
                    }
                }
            }
            if (time(NULL) - timeBefore > DEFAULT_TIMEOUT) {
                printf("timed out.\n");
                break;
            }
            delete receivedPacket;
        }
        delete tcpPacket;
        delete ipv6Packet;
        delete ethPacket;
    }
}

void enviaTCPHalfOpening(int start, int end) {

    for(int i = start; i<end; i++) {
        printf("Attack: Half-open at %d... ", i);

        TCPPacket *tcpPacket = new TCPPacket(DEFAULT_SOURCE_PORT, i, 0, 0);
        tcpPacket->setSyn(true);
        IPv6Packet *ipv6Packet = new IPv6Packet(ip_dest, ip_sour, tcpPacket);
        EthPacket *ethPacket = new EthPacket(mac_dest, mac_sour, ipv6Packet);
        // SEND SYN
        enviaPacote(ethPacket);

        long timeBefore = time(NULL);
        while(1) {
            // RECV
            EthPacket *receivedPacket = recebePacote();
            // IF SYN/ACK, same ports, same mac
            if (receivedPacket->iPv6Packet != NULL) { // eh ipv6
                if (receivedPacket->iPv6Packet->tcpPacket != NULL) { // eh tcp
                    if (receivedPacket->iPv6Packet->tcpPacket->isSyn() &&
                        receivedPacket->iPv6Packet->tcpPacket->isAck()&&
                        receivedPacket->isSameSource(mac_dest) &&
                        receivedPacket->iPv6Packet->tcpPacket->getSourcePort() == i &&
                        receivedPacket->iPv6Packet->tcpPacket->getDestinationPort() == DEFAULT_SOURCE_PORT) {

                        //SEND RST
                        tcpPacket = new TCPPacket(DEFAULT_SOURCE_PORT, i,
                                                  receivedPacket->iPv6Packet->tcpPacket->getAckNumber(), 0);
                        tcpPacket->setRst(true);
                        ipv6Packet = new IPv6Packet(ip_dest, ip_sour, tcpPacket);
                        ethPacket = new EthPacket(mac_dest, mac_sour, ipv6Packet);

                        enviaPacote(ethPacket);
                        printf("success.\n");
                        delete receivedPacket;
                        break;
                    }
                }
            }
            if (time(NULL) - timeBefore > DEFAULT_TIMEOUT) {
                printf("timed out.\n");
                break;
            }
            delete receivedPacket;
        }
        delete tcpPacket;
        delete ipv6Packet;
        delete ethPacket;
    }
}

void enviaTCPFin(int start, int end) {

    for(int i = start; i<end; i++) {
        printf("Attack: TCP FIN at %d... ", i);

        TCPPacket *tcpPacket = new TCPPacket(DEFAULT_SOURCE_PORT, i, 0, 0);
        tcpPacket->setFin(true);
        IPv6Packet *ipv6Packet = new IPv6Packet(ip_dest, ip_sour, tcpPacket);
        EthPacket *ethPacket = new EthPacket(mac_dest, mac_sour, ipv6Packet);
        // SEND FIN
        enviaPacote(ethPacket);

        long timeBefore = time(NULL);
        while(1) {
            // RECV
            EthPacket *receivedPacket = recebePacote();
            // IF RST, same ports, same mac
            if (receivedPacket->iPv6Packet != NULL) { // eh ipv6
                if (receivedPacket->iPv6Packet->tcpPacket != NULL) { // eh tcp
                    if (receivedPacket->iPv6Packet->tcpPacket->isRst() &&
                        receivedPacket->isSameSource(mac_dest) &&
                        receivedPacket->iPv6Packet->tcpPacket->getSourcePort() == i &&
                        receivedPacket->iPv6Packet->tcpPacket->getDestinationPort() == DEFAULT_SOURCE_PORT) {

                        printf("port is closed.\n");
                        delete receivedPacket;
                        break;
                    }
                }
            }
            if (time(NULL) - timeBefore > DEFAULT_TIMEOUT) {
                printf("timed out: port is OPEN.\n");
                break;
            }
            delete receivedPacket;
        }
        delete tcpPacket;
        delete ipv6Packet;
        delete ethPacket;
    }
}

void enviaSynAck(int start, int end) {

    for (int i = start; i < end; i++) {
        printf("Attack: SYN/ACK at %d... ", i);

        TCPPacket *tcpPacket = new TCPPacket(DEFAULT_SOURCE_PORT, i, 0, 0);
        tcpPacket->setSyn(true)->setAck(true);
        IPv6Packet *ipv6Packet = new IPv6Packet(ip_dest, ip_sour, tcpPacket);
        EthPacket *ethPacket = new EthPacket(mac_dest, mac_sour, ipv6Packet);
        // SEND SYN/ACK
        enviaPacote(ethPacket);
        long timeBefore = time(NULL);
        while (1) {
            // RECV
            EthPacket *receivedPacket = recebePacote();
            // IF RST, same ports, same mac
            if (receivedPacket->iPv6Packet != NULL) { // eh ipv6
                if (receivedPacket->iPv6Packet->tcpPacket != NULL) { // eh tcp
                    if (receivedPacket->isSameSource(mac_dest) &&
                        receivedPacket->iPv6Packet->tcpPacket->isRst() &&
                        receivedPacket->iPv6Packet->tcpPacket->getSourcePort() == i &&
                        receivedPacket->iPv6Packet->tcpPacket->getDestinationPort() == DEFAULT_SOURCE_PORT) {
                        printf("port is OPEN.\n");
                        delete receivedPacket;
                        break;
                    }
                }
            }
            if (time(NULL) - timeBefore > DEFAULT_TIMEOUT) {
                printf("timed out.\n");
                break;
            }
            delete receivedPacket;
        }
        delete tcpPacket;
        delete ipv6Packet;
        delete ethPacket;
    }
}

void detecta(char *macDest){
    int contSyn=0;
    int contTCPconnect = 0;
    int contTCPhalf = 0;
    int contTCPFin = 0;
    int contSynAck = 0;
    short int auxPort= 0;
    char *ipSour= (char*)malloc(16);

    EthPacket *receivedPacket;
    //coloca para modo promisc
//    strcpy(ifr.ifr_name, defaultInterface);
//    if(ioctl(sockrecv, SIOCGIFINDEX, &ifr) < 0)
//        perror("erro no ioctl!");
//    ioctl(sockrecv, SIOCGIFFLAGS, &ifr);
//    ifr.ifr_flags |= IFF_PROMISC;
//    ioctl(sockrecv, SIOCSIFFLAGS, &ifr);

    bool firstTime = true;
    while(1) {

        receivedPacket = recebePacote();

        if (receivedPacket->iPv6Packet!=NULL) {

            if (receivedPacket->iPv6Packet->tcpPacket!=NULL) {
                //Se o pacote recebido for para o mesmo mac e o mesmo mip envio o pacote
                if (memcmp(receivedPacket->getSource(), mac_sour, 6)==0) {
                    if (firstTime) {
                        memcpy(ipSour, receivedPacket->iPv6Packet->getSource(), 16);
                        firstTime = false;
                    }

                    if (memcmp(receivedPacket->iPv6Packet->getSource(), ipSour, 16) == 0 &&
                        (receivedPacket->iPv6Packet->tcpPacket->getDestinationPort() != auxPort)){
                        auxPort = receivedPacket->iPv6Packet->tcpPacket->getDestinationPort();
                        //verificador para ataques TCP Connect e TCP Half-Close
                        if (receivedPacket->iPv6Packet->tcpPacket->isSyn()
                            && !receivedPacket->iPv6Packet->tcpPacket->isAck()  ) {
                            contSyn = contSyn + 1;
                        }/*
                        //Verificador para ataques TCP Half-Close
                        if (receivedPacket->iPv6Packet->tcpPacket->isRst()) {
                            if (contSyn > 0) {
                                contSyn = contSyn - 1;
                                contTCPhalf = contTCPhalf + 1;
                            }
                        }
                        //Verificador para ataques TCP Connect
                        if (receivedPacket->iPv6Packet->tcpPacket->isAck()) {
                            if (contSyn > 0) {
                                contSyn = contSyn - 1;
                                contTCPconnect = contTCPconnect + 1;
                            }
                        }*/
                        //verificador para ataques TCP FIN
                        if (receivedPacket->iPv6Packet->tcpPacket->isFin()) {
                            contTCPFin = contTCPFin + 1;
                        }
                        //verificador para ataques SYN/ACK
                        if (receivedPacket->iPv6Packet->tcpPacket->isSyn() &&
                            receivedPacket->iPv6Packet->tcpPacket->isAck()) {
                            contSynAck = contSynAck+1;
                        }
                    }
                }
            }
        }


        if(contSyn>3){
            printf("%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx attacking %hhx:%hhx:%hhx:%hhx:%hhx:%hhx with TCP Connect or TCP Half-Close\n",
                   ipSour[0],ipSour[1],ipSour[2],ipSour[3],ipSour[4],ipSour[5],ipSour[6],ipSour[7],ipSour[8],ipSour[9],ipSour[10],ipSour[11],ipSour[12],ipSour[13],ipSour[14],ipSour[15],
                   macDest[0],macDest[1],macDest[2],macDest[3],macDest[4],macDest[5]);
            contSyn = 0;
        }
//        if(contTCPconnect>3){
//            printf("%s attacking %s with TCP Connect\n",ipSour, macDest);
//        }
//        if(contTCPhalf>3){
//            printf("%s attacking %s with TCP Half-Close\n",ipSour, macDest);
//        }
        if(contTCPFin>3){
            printf("%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx attacking %hhx:%hhx:%hhx:%hhx:%hhx:%hhx with TCP FIN\n",
                   ipSour[0],ipSour[1],ipSour[2],ipSour[3],ipSour[4],ipSour[5],ipSour[6],ipSour[7],ipSour[8],ipSour[9],ipSour[10],ipSour[11],ipSour[12],ipSour[13],ipSour[14],ipSour[15],
                   macDest[0],macDest[1],macDest[2],macDest[3],macDest[4],macDest[5]);
            contTCPFin = 0;
        }
        if(contSynAck>3){
            printf("%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx:%hhx%hhx attacking %hhx:%hhx:%hhx:%hhx:%hhx:%hhx with SYN/ACK\n",
                   ipSour[0],ipSour[1],ipSour[2],ipSour[3],ipSour[4],ipSour[5],ipSour[6],ipSour[7],ipSour[8],ipSour[9],ipSour[10],ipSour[11],ipSour[12],ipSour[13],ipSour[14],ipSour[15],
                   macDest[0],macDest[1],macDest[2],macDest[3],macDest[4],macDest[5]);
            contSynAck = 0;
        }

    }
}

void setMacFromString(char *dest, char *source) {
    if( 6 == sscanf( source, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
                     &dest[0],
                     &dest[1],
                     &dest[2],
                     &dest[3],
                     &dest[4],
                     &dest[5] ) ) {
        int i;
//        for(i = 0; i < 6; ++i )
//            dest[i] = (uint8_t) dest[i];
    } else {
        printf("Invalid MAC\n");
    }
}

int main(int argc, char ** argv) {

    if (argc<8) {
        printf("Usage: <ipv6_dest> <mac_dest> <ipv6_source> <mac_source> "\
                       "<typeATK> (1=TCP Connect| 2=TCP Half-open| 3=TCP FIN| 4=SYN-ACK) "\
                       "<typeOfProg> (1=ataque| 2=detector) "\
                       "<interface>\n");
        exit(1);
    }

    // Inicializa as variaveis globais de enderecos
    inet_pton(AF_INET6, argv[1], &ip_dest);
    setMacFromString(mac_dest, argv[2]);
    inet_pton(AF_INET6, argv[3], &ip_sour);
    setMacFromString(mac_sour, argv[4]);

    int tipo = atoi(argv[5]);
    int modo = atoi(argv[6]);
    defaultInterface = argv[7];
    int start = atoi(argv[8]);
    int end = atoi(argv[9]);

    // Inicializa struct de interface
    defaultSockAddr = (sockaddr_ll*)malloc(sizeof(struct sockaddr_ll));
    defaultSockAddr->sll_family = htons(PF_PACKET);
    defaultSockAddr->sll_protocol = htons(ETH_P_ALL);
    defaultSockAddr->sll_halen = 6;
    defaultSockAddr->sll_ifindex = if_nametoindex(defaultInterface);


    criaSockets();

    if (modo == 1) { // ATAQUE

        switch (tipo) { // (1=TCP Connect| 2=TCP Half-open| 3=TCP FIN| 4=SYN-ACK)
            case 1:
                enviaTCPConnect(start, end);
                break;
            case 2:
                enviaTCPHalfOpening(start, end);
                break;
            case 3:
                enviaTCPFin(start, end);
                break;
            case 4:
                enviaSynAck(start, end);
                break;
        }

    } else if (modo == 2) { // DETECTOR
        detecta(mac_dest);
    }
    return 0;
}


