//
// Created by Lucas Feijo on 6/18/16.
//

#ifndef REDES2_PACKET_H
#define REDES2_PACKET_H


class Packet {

public:
    virtual char * getPacket() = 0;

};


#endif //REDES2_PACKET_H
