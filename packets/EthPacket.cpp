//
// Created by Lucas Feijo on 6/18/16.
//

#include <cstring>
#include <cstdlib>
#include <net/ethernet.h>
#include <netinet/in.h>
#include "EthPacket.h"
#include "IPv6Packet.h"

char * EthPacket::getPacket() {
    char * packet = (char *)malloc(14+iPv6Packet->getLength());
    int pointer = 0;
    memcpy(packet+pointer, destination, 6);
    pointer+=6;
    memcpy(packet+pointer, source, 6);
    pointer+=6;
    uint16_t t = htons(type);
    memcpy(packet+pointer, &t, 2);
    pointer+=2;
    memcpy(packet+pointer, data, iPv6Packet->getLength());
    return packet;
}

EthPacket::EthPacket(char * destination, char * source, IPv6Packet *packet) {
    memcpy(this->destination, destination, 6);
    memcpy(this->source, source, 6);
    this->type = ETHERTYPE_IPV6;
    this->iPv6Packet = packet;
    //this->data = (char*)malloc(iPv6Packet->getLength());
    this->data = packet->getPacket();
}

EthPacket::EthPacket(char *packet, uint16_t size) {
    int pt = 0;
    memcpy(this->destination, packet+pt, 6);
    pt+=6;
    memcpy(this->source, packet+pt, 6);
    pt+=6;
    memcpy(&type, packet+pt, 2);
    type = ntohs(type);
    pt+=2;
    if (this->type==ETHERTYPE_IPV6) {
        size_t s = ((unsigned int)size)-((unsigned int)pt);
        char *nextPacket = (char*)malloc(s);
        memcpy(nextPacket, packet+pt, s);
        this->iPv6Packet = new IPv6Packet(nextPacket, s);
        this->data = this->iPv6Packet->getPacket();
    } else {
        //printf("%hhx",this->type);
//        if (this->type==ETHERTYPE_ARP ||
//                this->type==ETHERTYPE_IP) printf("Ignored packet\n");
    }
}

bool EthPacket::isSameSource(char *string) {
    return memcmp(this->source, string, 6)==0;
}


EthPacket::~EthPacket() {
    //free(this->data);
}