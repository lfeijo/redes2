//
// Created by Lucas Feijo on 6/18/16.
//

#ifndef REDES2_ETHPACKET_H
#define REDES2_ETHPACKET_H

#include <string>
#include "IPv6Packet.h"

class EthPacket {

    char destination[6];
    char source[6];
    uint16_t type;
    char *data;

public:

    IPv6Packet *iPv6Packet = NULL;

    EthPacket(char *destination, char *source, IPv6Packet *packet);
    EthPacket(char *packet, uint16_t size);


    virtual ~EthPacket();

    char * getPacket();

     char *getDestination()  {
        return destination;
    }

     char *getSource()  {
        return source;
    }

    short getType()  {
        return type;
    }

    bool isSameSource(char string[6]);
};


#endif //REDES2_ETHPACKET_H
