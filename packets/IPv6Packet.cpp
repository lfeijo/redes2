//
// Created by Lucas Feijo on 6/17/16.
//

#include <cstdlib>
#include <cstring>
#include <netinet/in.h>
#include "IPv6Packet.h"
#include "TCPPacket.h"

char * IPv6Packet::getPacket() {
    char * packet = (char *) malloc(length);
    int pointer = 0;
    uint8_t v = 6;
    v = v<<4;
    memcpy(packet+pointer, &v, 1);
    pointer+=1;
    uint32_t firstLine = 0x000000;
    memcpy(packet+pointer, &firstLine, 3);
    pointer+=3;
    uint16_t l = htons(20);
    memcpy(packet+pointer, &l, 2);
    pointer+=2;
    memcpy(packet+pointer, &nextHeader, 1);
    pointer+=1;
    memcpy(packet+pointer, &hopLimit, 1);
    pointer+=1;
    memcpy(packet+pointer, &source, 16);
    pointer+=16;
    memcpy(packet+pointer, &destination, 16);
    pointer+=16;
    memcpy(packet+pointer, data, TCPPacket::tcp_size);
    return packet;
}

IPv6Packet::IPv6Packet(char *destination, char *source, TCPPacket *packet) {
    memcpy(&(this->destination), destination, 16);
    memcpy(&(this->source), source, 16);
    this->data = (packet->getPacket(this->source, this->destination));
    this->type = 6;
    this->length = 40+TCPPacket::tcp_size;
    this->nextHeader = 6; // NO NEXT HEADER
    this->hopLimit = 64;
}

IPv6Packet::IPv6Packet(char *packet, uint16_t size) {
    int pt = 0;
    uint32_t firstLine = 0;
    memcpy(&firstLine, packet+pt, 4);
    // check version here
    pt+=4;
    memcpy(&length, packet+pt, 2);
    length = ntohs(length);
    pt+=2;
    memcpy(&nextHeader, packet+pt, 1);
    pt+=1;
    memcpy(&hopLimit, packet+pt, 1);
    pt+=1;
    memcpy(source, packet+pt, 16);
    pt+=16;
    memcpy(destination, packet+pt, 16);
    pt+=16;
    if (nextHeader!=59) { // se tiver header de extensao
        pt+=8; // avanca um header de extensao (ignoramos)
    }
    char *nextPacket = (char*)malloc(size-pt);
    memcpy(nextPacket, packet+40, 20);
    this->tcpPacket = new TCPPacket(nextPacket, 20);
    this->data = this->tcpPacket->getPacket(source, destination);
}


IPv6Packet::~IPv6Packet() {
    //free(data);
}