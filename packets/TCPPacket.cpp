//
// Created by Lucas Feijo on 6/17/16.
//

#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <netinet/in.h>
#include "TCPPacket.h"

#ifdef __APPLE__
#include <ntsid.h>
#endif

int TCPPacket::tcp_size = 4*5;
int TCPPacket::tcp_pseudo_size = 4*10;

TCPPacket::TCPPacket(uint16_t sourcePort, uint16_t destinationPort, uint32_t sequence, uint32_t ack) {
    this->sourcePort = sourcePort;
    this->destinationPort = destinationPort;
    this->sequenceNumber = sequence;
    this->ackNumber = ack;
}

uint16_t check (uint16_t *addr, size_t bytes) {
    unsigned int i;
    uint16_t *p = addr;
    uint32_t sum = 0;

    /* Sum */
    for (i=bytes; i > 1; i -= 2)
        sum += *p++;

    /* If uneven length */
    if (i > 0)
        sum += (uint16_t) *((unsigned char *) (p));

    /* Carry */
    while ((sum & 0xFFFF0000) != 0)
        sum = (sum >> 16) + (sum & 0xFFFF);

    return ~((uint16_t) sum);
}

TCPPacket::TCPPacket(char *packet, uint16_t size) {
    int pt = 0;
    memcpy(&sourcePort, packet+pt, 2);
    sourcePort = ntohs(sourcePort);
    pt+=2;
    memcpy(&destinationPort, packet+pt, 2);
    destinationPort = ntohs(destinationPort);
    pt+=2;
    memcpy(&sequenceNumber, packet+pt, 4);
    pt+=4;
    memcpy(&ackNumber, packet+pt, 4);
    pt+=4;
    uint8_t headerLen = 0;
    memcpy(&headerLen, packet+pt, 1);
    this->headerSize = headerLen >> 4;
    pt+=1;
    // flags:
    uint8_t flags = 0;
    memcpy(&flags, packet+pt, 1);
    ACK = (flags & 0b00010000)>0;
    RST = (flags & 0b00000100)>0;
    SYN = (flags & 0b00000010)>0;
    FIN = (flags & 0b00000001)>0;
    pt+=1;
    memcpy(&windowSize, packet+pt, 2);
    pt+=2;
    // checksum (ignored)
    pt+=2;
    // urgent pointer (ignored)
    pt+=2;
    if (headerLen>5) {
        pt += (headerLen-5)*4; // pula as palavras do campo de options
    }
//    char *nextPacket = (char*)malloc(size-pt);
//    memcpy(nextPacket, packet+pt, size-pt);
//    this->data = std::string(nextPacket);
}

char * TCPPacket::getPacket(char *sourceAddress, char *destinationAddress) {
    char * packet = (char *) malloc((tcp_pseudo_size)+(tcp_size)); // pseudo + header + data
    int chkPt = 0;
    int pt = 0;
    memcpy(packet+pt, sourceAddress, 16);
    pt+=16;
    memcpy(packet+pt, destinationAddress, 16);
    pt+=16;
    uint32_t headlen = htonl(tcp_size);
    memcpy(packet+pt, &headlen, 4);
    pt+=4;
    uint32_t zero = 0;
    memcpy(packet+pt, &zero, 3);
    pt+=3;
    uint8_t protocol = 6;
    memcpy(packet+pt, &protocol, 1);
    pt+=1;
    // fim pseudo
    uint16_t sp = htons(sourcePort);
    memcpy(packet+pt, &sp, 2);
    pt+=2;
    uint16_t dp = htons(destinationPort);
    memcpy(packet+pt, &dp, 2);
    pt+=2;
    uint32_t sn = htonl(sequenceNumber);
    memcpy(packet+pt, &sn, 4);
    pt+=4;
    uint32_t an = htonl(ackNumber);
    memcpy(packet+pt, &an, 4);
    pt+=4;
    uint8_t tam = this->headerSize;
    tam = tam << 4;
    memcpy(packet+pt, &tam, 1);
    pt+=1;
    uint8_t flag = 0;
    uint8_t curFlag = 0;

    curFlag = 0; // URG
    curFlag = curFlag << 5;
    flag |= curFlag;

    curFlag = this->ACK;
    curFlag = curFlag << 4;
    flag |= curFlag;

    curFlag = 0; // PSH
    curFlag = curFlag << 3;
    flag |= curFlag;

    curFlag = this->RST;
    curFlag = curFlag << 2;
    flag |= curFlag;

    curFlag = this->SYN;
    curFlag = curFlag << 1;
    flag |= curFlag;

    curFlag = this->FIN;
    //curFlag = curFlag << 0;
    flag |= curFlag;

    memcpy(packet+pt, &flag, 1);
    pt+=1;
    uint16_t ws = htons(windowSize);
    memcpy(packet+pt, &ws, 2);
    pt+=2;
    uint16_t checksum = 0;
    chkPt = pt;
    memcpy(packet+pt, &checksum, 2);
    pt+=2;
    uint16_t urgentPointer = 0;
    memcpy(packet+pt, &urgentPointer, 2);
    pt+=2;
    uint32_t options = 0;
    memcpy(packet+pt, &options, 4);
    pt+=4;

    checksum = check((uint16_t*)packet, tcp_pseudo_size+tcp_size);
    checksum = (checksum);
    memcpy(packet+chkPt, &checksum, 2);

    //char finalPacket[20];
    char * finalPacket = (char*)malloc(tcp_size);
    memcpy(finalPacket, packet+tcp_pseudo_size, pt-tcp_pseudo_size);
    return finalPacket;
}

TCPPacket * TCPPacket::setAck(bool ack) {
    this->ACK = ack;
    return this;
}

TCPPacket * TCPPacket::setRst(bool rst) {
    this->RST = rst;
    return this;
}

TCPPacket * TCPPacket::setSyn(bool syn) {
    this->SYN = syn;
    return this;
}

TCPPacket * TCPPacket::setFin(bool fin) {
    this->FIN = fin;
    return this;
}


TCPPacket::~TCPPacket() {

}