//
// Created by Lucas Feijo on 6/17/16.
//

#ifndef REDES2_TCPPACKET_H
#define REDES2_TCPPACKET_H

#include <string>

class TCPPacket {

    uint16_t sourcePort;
    uint16_t destinationPort;
    uint32_t sequenceNumber;
    uint32_t ackNumber;

    bool ACK = false;
    bool RST = false;
    bool SYN = false;
    bool FIN = false;

    uint8_t headerSize = 5; // in words
    uint16_t windowSize = UINT16_MAX; // in bytes

public:

    static int tcp_size;
    static int tcp_pseudo_size;

    TCPPacket(uint16_t sourcePort, uint16_t destinationPort, uint32_t sequence, uint32_t ack);
    TCPPacket(char *packet, uint16_t size);


    virtual ~TCPPacket();

    TCPPacket * setAck(bool ack);
    TCPPacket * setRst(bool rst);
    TCPPacket * setSyn(bool syn);
    TCPPacket * setFin(bool fin);

    bool isAck() { return ACK; }
    bool isRst() { return RST; }
    bool isSyn() { return SYN; }
    bool isFin() { return FIN; }

    char *getPacket(char *sourceAddress, char *destinationAddress);

    uint32_t getAckNumber() const { return ackNumber; }
    uint32_t getSequenceNumber() const { return sequenceNumber; }
    uint16_t getDestinationPort() const { return destinationPort; }
    uint16_t getSourcePort() const { return sourcePort; }

};


#endif //REDES2_TCPPACKET_H
