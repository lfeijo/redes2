//
// Created by Lucas Feijo on 6/17/16.
//

#ifndef REDES2_IPV6HEADER_H
#define REDES2_IPV6HEADER_H


#include <cstdint>
#include <string>
#include "TCPPacket.h"

class IPv6Packet {

    uint8_t type = 6;
    uint16_t length = 0;
    uint8_t nextHeader = 6;
    uint8_t hopLimit = 64;
    char destination[16];
    char source[16];
    char * data;

public:

    TCPPacket *tcpPacket = NULL;

    IPv6Packet(char *destination, char *source, TCPPacket *data);
    IPv6Packet(char *packet, uint16_t size);


    virtual ~IPv6Packet();

    char * getPacket();

     char *getDestination()  {
        return destination;
    }

     char *getSource()  {
        return source;
    }

    uint8_t getHopLimit()  {
        return hopLimit;
    }

    uint16_t getLength()  {
        return length;
    }

    uint8_t getType()  {
        return type;
    }

    uint8_t getNextHeader()  {
        return nextHeader;
    }
};


#endif //REDES2_IPV6HEADER_H
